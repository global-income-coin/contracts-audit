# Contracts

## Requirements:

Node: v16.41.2 LTS

## Description:

This repo contains the contracts used by Global Income Coin.

Installation: `npm install`
Run tests: `npx hardhat test`

## Contracts:

### contracts/glo/GLO.sol

This is the main ERC20 token contract deployed on Ethereum. It's only purpose is to mint new GLO tokens.

### contracts/polygon/child/ChildERC20.sol

This is the custom child contract which will be deployed on Polygon. This contract will be mapped to the main GlobalIncomeCoin.sol contract on Polygon. Minted ERC20 GLO tokens on the mainnet will be sent to the Polygon bridge. This ChildContract will instead mint wrapped GLO(WGLO) on Polygon corresponding to the received GLO.

### polygon/Register.sol

This is the register contract which will be deployed on Polygon. Verifiers can register humans on this contract.

### polygon/Distributor.sol

This is the distributor contract which will be deployed on Polygon. This contract will also contain functionality to create new WGLO drops and contain the actual distribution logic.

## How does a distribution happen?

The following steps are followed:

1. A verifier registers a user on the register contract.
2. Create a new drop on the distribution contract.
3. Mint the required quantity of GLO required for the above drop on the mainnet.
4. Approve and deposit this GLO to the child contract on Polygon using the Polygon bridge. This deposit should be to the address of the distribution contract on Polygon.
5. Start the WGLO distribution on Polygon by calling the corresponding distribution function on the distributor. Keep calling the function until all the users get their share.

## Helper talks/scripts to do the above:

### Register and verify a human:

`npx hardhat register --network polygon --contract registerContractAddress --human-id humanId --human-address walletAddress --metadata-urls file1url,file2url,file3url --last-verified-time lastVerifiedTimeInEpochSeconds`

### Update a human:

`npx hardhat update --network polygon --contract contractAddress --human-id humanId --human-address walletAddress --metadata-urls file1url,file2url,file3url`

### Reverify a human:

`npx hardhat reverify --network polygon --contract registerContractAddress --human-id humanId --last-verified-time lastVerifiedTimeInEpochSeconds`

### Unverify a human:

`npx hardhat unverify --network polygon --contract registerContractAddress --human-id humanId`

### Check if human verified:

`npx hardhat isVerified --network polygon --contract registerContractAddress --human-id humanId`

### Get balance of a user:

`npx hardhat balance --network polygon --contract childContractAddress --address walletAddress`

### Mint new GLO tokens on the mainnet:

`npx hardhat mint --network goerli --contract mainGLOContractAddress --to addressWhichWillApproveAndDepositGLOToPolygon --amount numOfTokens`

### Appove and Deposit GLO to Polygon to make WGLO:

`node scripts/depositToPolygon.ts`
Modify the script to point to correct addresses and number of tokens.

### Create a new drop:

`npx hardhat addDrop --network polygon --contract distributionContractAddress --drop-per-address numOfTokensPerUser`

### Start a distribution run:

`npx hardhat runDrop --network polygon --contract distributionContractAddress --drop-index dropIndexWhichYouWantToDistribute --address-count numberOfAddressesToGetDistributions`

### Withdraw WGLO from Polygon to make GLO:

`node scripts/withdrawFromPolygon.ts`
Modify the script to point to correct addresses and number of tokens.

### Grant role to address for contract:

`npx hardhat grantRole --network polygon --contract-address contractAddress --contract-name contractName --role-name roleName --address walletAddress`

### Revoke role from address for contract:

`npx hardhat revokeRole --network polygon --contract-address contractAddress --contract-name contractName --role-name roleName --address walletAddress`

### Check role for address for contract:

`npx hardhat hasRole --network polygon --contract-address contractAddress --contract-name contractName --role-name roleName --address walletAddress`

# More detailed explanation of Contracts

**Note:**

1. user = human
2. distribution = drop

The system currently comprises of **4 contracts**:

1. GLO ERC20 token
2. Polygon Child
3. Register
4. Distributor

### GLO ERC20 token

[See code](https://gitlab.com/global-income-coin/contracts/-/blob/main/contracts/GlobalIncomeCoin.sol)

This is a standard ERC20 token with a few modifications. This will be our main **GLO** token and will be deployed on the Ethereum main-net.

The contract will **inherit** from the following:

1. `ERC20` → This makes it an ERC20 token. The associated `mint` function can only be called if the address has `MINTER_ROLE` role in the contract.
2. `ERC20Burnable` → This gives the contract the capability to burn GLO. But we modify the permissions here. The associated `burn` and `burnFrom` functions can only be called if the address has `BURNER_ROLE` role in the contract. i.e. not everyone holding GLO can burn their tokens. Initially we just want the GIC foundation to be able to burn GLO. But we are not completely sure of what other use-cases we might want to explore here.
3. `AccessControl` → This will give the contract role based access control functionality. This contract will have the following roles:

   1. `DEFAULT_ADMIN_ROLE` → This will be the all encompassing admin role. Addresses with this role can grant other addresses other roles.
   2. `MINTER_ROLE` → Only addresses with this role can **mint** new GLO.
   3. `BURNER_ROLE` → Only addresses with this role can **burn** GLO.

   **Note:** All 3 roles described above are granted on contract creation to the address which created the contract.

The contract also has an **extra** function:

1. `transferMany` → This is a new function to efficiently bulk transfer tokens to multiple addresses. Let’s say we want to transfer 10 GLO each to 10 different accounts. We don’t want to call the usual `transfer` function 10 times as that causes 10 transactions and spends a lot of gas. `transferMany` will efficiently transfer to multiple addresses in 1 transaction.

   The current version of `transferMany` in the linked code is not the best way to do it. Instead we will be modifying the ERC20 class itself and adding the function there as we get direct access to the `_balances` mapping there. The logic involved will be described later when we describe the `Polygon Child` contract.

   Currently we only need this function on polygon. But we will add it to the main token on Ethereum anyway anticipating the mainnet chain becoming faster and cheaper in the future.

**Note:** This is an upgradable contract.

### Polygon Child

[See code](https://gitlab.com/global-income-coin/contracts/-/blob/main/contracts/polygon/ChildContract.sol)

How the polygon bridge works is quite simple. Polygon maintains a **root** contract on ethereum and a **child** contract on the polygon chain. If we want to transfer **x** GLO to polygon, the following set of events happen:

1. We let the **root** contract on ethereum take ownership of the **x** GLO.
2. The **child** contract on polygon now mints **x** new GLO for the polygon chain. This child contract is an ERC20 contract too.

So essentially polygon mints a new token on its chain for every token it takes ownership of on ethereum. If you want to transfer **x** GLO back from polygon to the ethereum chain, the child contract burns **x** GLO on the polygon side and the root contract releases **x** GLO back to you on the ethereum side. So it’s essentially a 1:1 mapping between the actual GLO token on ethereum and the GLO token polygon minted for the polygon chain.

We use a slightly modified version of the standard ERC20 child contract used by polygon. We add the `transferMany` function to the contract just like we added it to the GLO ERC20 token as described in the section above. This is to enable cheaper airdropping of GLO to humans.

**transferMany:**

The optimisation here is the fact that the balance of the sender is only updated at the end once the balance of all the recipients have been updated. Storage slot updates are costly and and doing it this way reduces the amount of gas used by a lot.

```solidity
/**
 * @dev Optimised bulk transfer from sender to multiple recipients.
 *
 * Requirements:
 *
 * - `recipients` can contain zero address but they will be ignored.
 * - `amount` is the quantity transfered to each non-zero recipient address.
 * - the caller must have a balance of at least `amount` * number of non-zero `recipients`.
 */
function transferMany(address[] calldata recipients, uint256 amount)
  external
  returns (bool)
{
  address sender = _msgSender();
  uint256 senderBalance = _balances[sender];

  for (uint256 index = 0; index < recipients.length; ++index) {
    address recipient = recipients[index];

    if (recipient != address(0)) {
      _beforeTokenTransfer(sender, recipient, amount);
      senderBalance = senderBalance.sub(
        amount,
        "ERC20: transfer amount exceeds balance"
      );
      _balances[recipient] = _balances[recipient].add(amount);
      emit Transfer(sender, recipient, amount);
    }
  }
  _balances[sender] = senderBalance;
  return true;
}

```

**Note:** This is not an upgradable contract.

### Register

[See code](https://gitlab.com/global-income-coin/contracts/-/blob/main/contracts/polygon/Register.sol)

A Register contract allows you to register new users to receive weekly GLO distributions.

The contract will **inherit** from the following:

1. `AccessControl` → This will give the contract role based access control functionality. This contract will have the following roles:

1. `DEFAULT_ADMIN_ROLE` → This will be the all encompassing admin role. Addresses with this role can grant other addresses other roles. Addresses with this role can also unverify any user. Address with this role can also set verificationValidity in seconds.
1. `REGISTER_ROLE` → Addresses with this role will be allowed to:
   1. Register new users.
   2. Update user information if the user is unverified or if the same address last registered/verified the user.
   3. Reverify users provided the same address last registered/verified the user.
   4. Unverify users provided the same address last registered/verified the user.

The contract has the following functions:

1. `updatedVerificationValiditySeconds` → Updates in seconds how long a human verification is valid for.
2. `registerHuman` → Register a human to receive new GLO distributions. This accepts a **humanId**(arbitrary id representing a human, this id will be created and maintained by verifiers), **humanAddress**(wallet address which will receive the GLO), **metadataUrls**(things like Arweave url’s containing human PII data to be used for deduplication) and **lastVerifiedTime**(epoch in seconds representing when the human was verified).
3. `updateHuman` → Update a registered human. This accepts a **humanId**(arbitrary id representing a human, this id will be created and maintained by verifiers) and **humanAddress**(wallet address which will receive the GLO), **metadataUrls**(things like Arweave url’s containing human PII data to be used for deduplication).
4. `reverifyHuman` → Reverifies a human by updating **isVerified** to **true** and updates **lastVerifiedTime** to given **lastVerifiedTime** for the given **humanId**.
5. `unverifyHuman` → Unverifies a human by updating **isVerified** to **false** for the given **humanId**.
6. `isHumanVerified` → Accepts a **Human** object as an argument and returns whether that human is verified or not. A human needs to have the _isVerified_ field set to _true_ by some verifier **and** needs its _lastVerifiedTime_ to be within valid verification time-block(very old verifications will no longer be considered as verified).
7. `isHumanIdVerified` → External version of the above. Accepts a **human** id instead of a **Human** object.
8. `getRegisteredHumansCount` → Returns count of registered humans. This count includes unverified humans too(cannot return count of just verified humans accurately as that changes dynamically depending on block timestamp.).
9. `getAddressesSlice` → Return slice of addresses registered and verified. Addresses returned can be 0 value if human corresponding to it is no longer verified. Duplicate addresses can be returned as multiple verified humans can have the same address. It accepts a **startIndex**(inclusive) and a **endIndex**(exclusive). The Distributor contract will keep track of indexes it has distributed GLO to and use this function to get the next set of registered addresses who should receive GLO.

**Note:** This is an upgradable contract.

### Distributor

[See code](https://gitlab.com/global-income-coin/contracts/-/blob/main/contracts/polygon/Distributor.sol)

A Distributor contract allows you to add a new distribution (in our case weekly), holds funds required for distributions and contains functions to actually funds distribute among addresses.

The contract will **inherit** from the following:

1. `AccessControl` → This will give the contract role based access control functionality. This contract will have the following roles:

1. `DEFAULT_ADMIN_ROLE` → This will be the all encompassing admin role. Addresses with this role can grant other addresses other roles.
1. `DISTRIBUTOR_ROLE` → Addresses with this role will be allowed to:
   1. Add a new distribution.
   2. Run an existing distribution, i.e. distribute among users.

The contract has to be **initialised** with the following:

1. `globalIncomeCoinAddress` → Address of GLO token. In our case this will be the Polygon child contract.
2. `registerAddress` → Address of the Register contract. The users eligible for GLO airdrops when a distribution is created, fetching users for actual transfers are all queried from this register address.

The contract has the following functions:

1. `addDrop` → Create a new distribution. It requires the following arguments:
   1. `_dropPerHuman` → The amount of GLO which each user should receive from this distribution.
2. `runDrop` → Run a distribution. This function is what actually transfers GLO to eligible users. It requires the following arguments:

   1. `_dropIndex` → The index(id) of the distribution you want to run. You get this when you run `addDrop`.
   2. `_humanCount` → The number of users who should receive transfers during this run of the distribution.

   This function works very similarly to generators in python. It maintains an internal state of iteration of a particular distribution. Keep calling this function with a particular **\_dropIndex** until the last user has received their GLO. Calling again after that raises an error.

**Note:** This is an upgradable contract.
