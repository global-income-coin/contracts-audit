// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

interface ITransferManyToken {
    function transferMany(address[] calldata recipients, uint256 amount)
        external
        returns (bool);
}
