// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import {ITransferManyToken} from "../ITransferManyToken.sol";
import {Register} from "./Register.sol";
import "openzeppelin-contracts-upgradeable-4.6.0/access/AccessControlUpgradeable.sol";
import "openzeppelin-contracts-upgradeable-4.6.0/proxy/utils/Initializable.sol";
import "openzeppelin-contracts-upgradeable-4.6.0/proxy/utils/UUPSUpgradeable.sol";

error BadDropPerHuman(uint256 dropPerHuman);
error NonExistingDrop(uint256 dropIndex);
error BadHumanCount(uint256 humanCount);
error DropAlreadyCompleted(uint256 dropIndex);

/**
 * @title Distributor
 * @notice This contract serves as a way to distribute GLO to active registered users.
 *         Active registered humans will queried from the Register contract.
 */
contract Distributor is
    Initializable,
    AccessControlUpgradeable,
    UUPSUpgradeable
{
    event AddDrop(
        uint256 indexed dropIndex,
        uint256 dropPerHuman,
        uint128 totalHumans,
        uint128 nextHumanIndex
    );
    event RunDrop(
        uint256 indexed dropIndex,
        uint256 dropPerHuman,
        uint128 totalHumans,
        uint128 nextHumanIndex
    );

    // Only this role will be allowed to add and run distribution drops.
    bytes32 public constant DISTRIBUTOR_ROLE = keccak256("DISTRIBUTOR_ROLE");
    // Only this role will be allowed to upgrade this contract.
    bytes32 public constant UPGRADER_ROLE = keccak256("UPGRADER_ROLE");

    // ERC20 token contract of GLO.
    ITransferManyToken public globalIncomeCoin;
    // Register contract keeping track of humans.
    Register public register;

    // A drop represents a airdrop for every registered active human.
    // If GLO is distributed to everyone weekly, a new
    // Drop object would need to be created every week.
    struct Drop {
        // Token quantity per human.
        uint256 dropPerHuman;
        // Total registered humans as of Drop creation.
        uint128 totalHumans;
        // Index keeping track of progress of token distribution
        // to registered humans in Drop object.
        // This index will point to the next human who will receive
        // their airdrop when Drop distribution is run.
        uint128 nextHumanIndex;
    }

    // Array of all created Drops.
    Drop[] public drops;

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }

    /**
     * @notice In the initialize, give the deployer both admin role and register role.
     */
    function initialize(
        address globalIncomeCoinAddress,
        address registerAddress
    ) public initializer {
        __AccessControl_init();
        __UUPSUpgradeable_init();

        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(DISTRIBUTOR_ROLE, msg.sender);
        _grantRole(UPGRADER_ROLE, msg.sender);

        globalIncomeCoin = ITransferManyToken(globalIncomeCoinAddress);
        register = Register(registerAddress);
    }

    /**
     * @notice Add a new Drop object to the drops array.
     *
     * @param _dropPerHuman Quantity of token to be distributed per human in drop.
     */
    function addDrop(uint256 _dropPerHuman)
        external
        onlyRole(DISTRIBUTOR_ROLE)
        returns (uint256 addedDropIndex)
    {
        if (_dropPerHuman == 0) {
            revert BadDropPerHuman({dropPerHuman: _dropPerHuman});
        }

        // Casting to uint128 is ok as the number of humans will never be bigger than that.
        uint128 totalHumans = uint128(register.getRegisteredHumansCount());
        Drop memory drop = Drop({
            dropPerHuman: _dropPerHuman,
            totalHumans: totalHumans,
            nextHumanIndex: 0
        });
        drops.push(drop);
        unchecked {
            addedDropIndex = drops.length - 1;
        }

        emit AddDrop(addedDropIndex, _dropPerHuman, totalHumans, 0);
    }

    /**
     * @notice Run a created Drop.
     *
     * @param _dropIndex Index of Drop object to be run in the drops array.
     * @param _humanCount Count of humans who should receive drop in this particular run of the drop.
     */
    function runDrop(uint256 _dropIndex, uint128 _humanCount)
        external
        onlyRole(DISTRIBUTOR_ROLE)
        returns (bool ranDrop)
    {
        if (_dropIndex >= drops.length) {
            revert NonExistingDrop({dropIndex: _dropIndex});
        }
        if (_humanCount == 0) {
            revert BadHumanCount({humanCount: _humanCount});
        }

        Drop storage drop = drops[_dropIndex];

        uint128 currentNextHumanIndex = drop.nextHumanIndex;
        uint128 totalHumans = drop.totalHumans;
        if (currentNextHumanIndex >= totalHumans) {
            revert DropAlreadyCompleted({dropIndex: _dropIndex});
        }

        uint128 newNextHumanIndex = currentNextHumanIndex + _humanCount;

        // Get and update the nextHumanIndex of the drop for the next run of this method.
        // If next index is out of bounds, set to first out-of-bounds index.
        if (newNextHumanIndex >= totalHumans) {
            newNextHumanIndex = totalHumans;
        }
        drop.nextHumanIndex = newNextHumanIndex;

        // Get wallet addresses who should receive tokens for this run of the distribution drop.
        address[] memory dropAddresses = register.getAddressesSlice(
            currentNextHumanIndex,
            newNextHumanIndex
        );

        uint256 dropPerHuman = drop.dropPerHuman;

        // Use the optimised custom transferMany method.
        ranDrop = globalIncomeCoin.transferMany(dropAddresses, dropPerHuman);

        emit RunDrop(_dropIndex, dropPerHuman, totalHumans, newNextHumanIndex);
    }

    function _authorizeUpgrade(address newImplementation)
        internal
        override
        onlyRole(UPGRADER_ROLE)
    {}
}
