// SPDX-License-Identifier: MIT
pragma solidity 0.8.15;

import "openzeppelin-contracts-upgradeable-4.6.0/access/AccessControlUpgradeable.sol";
import "openzeppelin-contracts-upgradeable-4.6.0/proxy/utils/Initializable.sol";
import "openzeppelin-contracts-upgradeable-4.6.0/proxy/utils/UUPSUpgradeable.sol";
import "openzeppelin-contracts-upgradeable-4.6.0/utils/StringsUpgradeable.sol";

error HumanAlreadyRegistered(string humanId);
error HumanNotRegistered(string humanId);
error NotCurrentVerifierOfHuman(address verifier, string humanId);
error NotCurrentVerifierOfHumanOrAdmin(address verifier, string humanId);
error BadUserQueryIndexes(uint256 startIndex, uint256 endIndex);
error BadLastVerifiedTime(uint256 lastVerifiedTime);

/**
 * @title Register
 * @notice This contract serves as a way to verify and register humans to receive GLO.
 *         Verified registered humans will queried from this Register contract
 *         and distributed GLO by another Distributor contract.
 */
contract Register is Initializable, AccessControlUpgradeable, UUPSUpgradeable {
    event NewRegistration(
        string indexed humanId,
        address indexed walletAddress,
        string[] metadataUrls,
        bool isVerified,
        address lastVerifiedBy,
        uint256 lastVerifiedTime
    );
    event UpdatedRegistration(
        string indexed humanId,
        address indexed walletAddress,
        string[] metadataUrls,
        bool isVerified,
        address lastVerifiedBy,
        uint256 lastVerifiedTime
    );
    event Reverified(
        string indexed humanId,
        address indexed walletAddress,
        string[] metadataUrls,
        bool isVerified,
        address lastVerifiedBy,
        uint256 lastVerifiedTime
    );
    event Unverified(
        string indexed humanId,
        address indexed walletAddress,
        string[] metadataUrls,
        bool isVerified,
        address lastVerifiedBy,
        uint256 lastVerifiedTime
    );

    // Only this role will be allowed to register new humans
    // and update humans who are either unverified or last verified by them.
    bytes32 public constant REGISTER_ROLE = keccak256("REGISTER_ROLE");
    // Only this role will be allowed to reverify humans verified last by them.
    bytes32 public constant REVERIFY_ROLE = keccak256("REVERIFY_ROLE");
    // Only this role will be allowed to unverify humans verified last by them.
    bytes32 public constant UNVERIFY_ROLE = keccak256("UNVERIFY_ROLE");
    // Only this role will be allowed to upgrade this contract.
    bytes32 public constant UPGRADER_ROLE = keccak256("UPGRADER_ROLE");

    // Struct representing every registered human.
    struct Human {
        // Address where human will receive GLO airdrops.
        address walletAddress;
        // Represents if a human is still verified.
        // Will be set to false in case of unverification.
        // This field set to true does not mean human is verified.
        // It just means that human was previously verified
        // and can be reverified in the event that verification has expired.
        // Use isHumanIdVerified or isHumanVerified
        // to check actual human verification status.
        bool isVerified;
        // Offchain urls pointing to human metadata.
        string[] metadataUrls;
        // Last verified by address.
        address lastVerifiedBy;
        // Timestamp when human was last verified.
        uint256 lastVerifiedTime;
    }

    // Unique Human Id to Human object mapping for registered human.
    mapping(string => Human) public humansMap;

    // Array of Human Id's corresponding to registered humans.
    // Human Id will not be removed in case of deverification.
    string[] public humanIdArray;

    // Validity of a human verification in seconds.
    // Be sure to add seconds to cover possible manipulation of block timestamp.
    uint256 public humanVerificationValidityInSeconds;

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() {
        _disableInitializers();
    }

    /**
     * @notice In the initialize, give the deployer both admin role and register role.
     */
    function initialize() public initializer {
        __AccessControl_init();
        __UUPSUpgradeable_init();

        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(REGISTER_ROLE, msg.sender);
        _grantRole(REVERIFY_ROLE, msg.sender);
        _grantRole(UNVERIFY_ROLE, msg.sender);
        _grantRole(UPGRADER_ROLE, msg.sender);
    }

    /**
     * @notice Update validity period of a human verification in seconds.
     *
     * @param _humanVerificationValidityInSeconds Validity period in seconds.
     */
    function updateVerificationValiditySeconds(
        uint256 _humanVerificationValidityInSeconds
    )
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
        returns (bool updatedVerificationValiditySeconds)
    {
        humanVerificationValidityInSeconds = _humanVerificationValidityInSeconds;
        updatedVerificationValiditySeconds = true;
    }

    /**
     * @notice Register and verify a new human.
     *
     * @param humanId Unique ID representing the human.
     * @param humanAddress Address where the human should receive GLO distributions.
     * @param metadataUrls Urls pointing to offchain metadata file corresponding to human being registered.
     * @param lastVerifiedTime Epoch in seconds representing when the human was verified.
     */
    function registerHuman(
        string calldata humanId,
        address humanAddress,
        string[] calldata metadataUrls,
        uint256 lastVerifiedTime
    ) external onlyRole(REGISTER_ROLE) returns (bool registeredHuman) {
        // Require that human has never been registered.
        if (humansMap[humanId].lastVerifiedTime > 0) {
            revert HumanAlreadyRegistered({humanId: humanId});
        }
        // Require that lastVerifiedTime is not 0.
        if (lastVerifiedTime == 0) {
            revert BadLastVerifiedTime({lastVerifiedTime: lastVerifiedTime});
        }

        Human memory human = Human({
            walletAddress: humanAddress,
            metadataUrls: metadataUrls,
            isVerified: true,
            lastVerifiedBy: _msgSender(),
            lastVerifiedTime: lastVerifiedTime
        });
        humansMap[humanId] = human;
        humanIdArray.push(humanId);

        registeredHuman = true;

        emit NewRegistration(
            humanId,
            humanAddress,
            metadataUrls,
            true,
            _msgSender(),
            lastVerifiedTime
        );
    }

    /**
     * @notice Update a human.
     *
     * @param humanId Unique ID representing the human.
     * @param humanAddress Address where the human should receive GLO distributions.
     * @param metadataUrls Urls pointing to offchain metadata file corresponding to human being registered.
     */
    function updateHuman(
        string calldata humanId,
        address humanAddress,
        string[] memory metadataUrls
    ) external onlyRole(REGISTER_ROLE) returns (bool updatedHuman) {
        Human storage human = humansMap[humanId];

        // Allow update only if human is already registered and:
        // 1. Human being updated is verified and the caller is their current verifier.
        // OR
        // 2. Human being updated is unverified(ignoring expiry).
        bool isVerified = human.isVerified;
        if (_msgSender() != human.lastVerifiedBy && isVerified) {
            revert NotCurrentVerifierOfHuman({
                verifier: _msgSender(),
                humanId: humanId
            });
        }

        // Require that human is registered.
        uint256 lastVerifiedTime = human.lastVerifiedTime;
        if (lastVerifiedTime == 0) {
            revert HumanNotRegistered({humanId: humanId});
        }

        human.lastVerifiedBy = _msgSender();
        human.walletAddress = humanAddress;
        human.metadataUrls = metadataUrls;

        updatedHuman = true;

        emit UpdatedRegistration(
            humanId,
            humanAddress,
            metadataUrls,
            isVerified,
            _msgSender(),
            lastVerifiedTime
        );
    }

    /**
     * @notice Reverify a human.
     *
     * @param humanId Id of human who needs to be reverified.
     * @param lastVerifiedTime Epoch in seconds representing when the human was verified.
     */
    function reverifyHuman(string calldata humanId, uint256 lastVerifiedTime)
        external
        onlyRole(REVERIFY_ROLE)
        returns (bool reverifiedHuman)
    {
        Human storage human = humansMap[humanId];

        // Require that human is registered.
        if (human.lastVerifiedTime == 0) {
            revert HumanNotRegistered({humanId: humanId});
        }

        // Allow reverification only if caller is their current verifier.
        if (_msgSender() != human.lastVerifiedBy) {
            revert NotCurrentVerifierOfHuman(_msgSender(), humanId);
        }

        // Require that lastVerifiedTime is not 0.
        if (lastVerifiedTime == 0) {
            revert BadLastVerifiedTime({lastVerifiedTime: lastVerifiedTime});
        }

        human.isVerified = true;
        human.lastVerifiedTime = lastVerifiedTime;

        emit Reverified(
            humanId,
            human.walletAddress,
            human.metadataUrls,
            true,
            _msgSender(),
            lastVerifiedTime
        );

        reverifiedHuman = true;
    }

    /**
     * @notice Unverify a human.
     *
     * @param humanId Id of human who needs to be unverified.
     */
    function unverifyHuman(string calldata humanId)
        external
        returns (bool unverifiedHuman)
    {
        Human storage human = humansMap[humanId];

        // Require that caller is admin or current verifier of human.
        address lastVerifiedBy = human.lastVerifiedBy;
        if (
            !(hasRole(UNVERIFY_ROLE, _msgSender()) &&
                _msgSender() == lastVerifiedBy) &&
            !(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()))
        ) {
            revert NotCurrentVerifierOfHumanOrAdmin({
                verifier: _msgSender(),
                humanId: humanId
            });
        }

        uint256 lastVerifiedTime = human.lastVerifiedTime;
        // Require that human is registered.
        if (lastVerifiedTime == 0) {
            revert HumanNotRegistered({humanId: humanId});
        }

        human.isVerified = false;

        emit Unverified(
            humanId,
            human.walletAddress,
            human.metadataUrls,
            false,
            lastVerifiedBy,
            lastVerifiedTime
        );

        unverifiedHuman = true;
    }

    /**
     * @notice Returns if a humanId is registered and verified.
     *
     * @param humanId Human whose verification is checked.
     *
     * @return isVerified Boolean indicating if humanId is verified.
     */
    function isHumanIdVerified(string calldata humanId)
        external
        view
        returns (bool isVerified)
    {
        Human storage human = humansMap[humanId];
        isVerified = isHumanVerified(human);
    }

    /**
     * @notice Returns if a Human is registered and verified.
     *
     * @param human Human whose verification is checked.
     *
     * @return isVerified Boolean indicating if human is verified.
     */
    function isHumanVerified(Human storage human)
        internal
        view
        returns (bool isVerified)
    {
        if (human.isVerified) {
            if (
                human.lastVerifiedTime + humanVerificationValidityInSeconds >
                block.timestamp
            ) {
                isVerified = true;
            }
        }
    }

    /**
     * @notice Returns count of registered humans.
     *         This count includes unverified humans too.
     *
     * @return registeredHumansCount Count of registered humans.
     */
    function getRegisteredHumansCount()
        public
        view
        returns (uint256 registeredHumansCount)
    {
        registeredHumansCount = humanIdArray.length;
    }

    /**
     * @notice Return slice of addresses registered and verified.
     *         Addresses returned can be 0 value if human corresponding to it is no longer verified.
     *         Duplicate addresses can be returned as multiple verified humans can have the same address.
     *
     * @param startIndex Start index. This index is inclusive.
     * @param endIndex   End index. This index is exclusive.
     *
     * @return addressesSlice Addresses belonging to queried indexes.
     */
    function getAddressesSlice(uint256 startIndex, uint256 endIndex)
        external
        view
        returns (address[] memory addressesSlice)
    {
        // Validate query indexes.
        if (startIndex > endIndex) {
            revert BadUserQueryIndexes({
                startIndex: startIndex,
                endIndex: endIndex
            });
        }
        unchecked {
            addressesSlice = new address[](endIndex - startIndex);
        }
        for (uint256 index = startIndex; index < endIndex; ) {
            Human storage human = humansMap[humanIdArray[index]];
            if (isHumanVerified(human)) {
                unchecked {
                    addressesSlice[index - startIndex] = human.walletAddress;
                }
            }

            unchecked {
                ++index;
            }
        }
    }

    /**
     * @notice Return metadataUrls of a human.
     *
     * @param humanId Unique ID representing the human.
     *
     * @return metadataUrls metadataUrls belonging to queried human.
     */
    function getMetadataUrls(string calldata humanId)
        external
        view
        returns (string[] memory metadataUrls)
    {
        metadataUrls = humansMap[humanId].metadataUrls;
    }

    function _authorizeUpgrade(address newImplementation)
        internal
        override
        onlyRole(UPGRADER_ROLE)
    {}
}
