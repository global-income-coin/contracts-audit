require('dotenv').config();

let { POSClient, use, setProofApi } = require("@maticnetwork/maticjs");
let { Web3ClientPlugin } = require("@maticnetwork/maticjs-ethers");
let { providers, Wallet, utils } = require("ethers");

use(Web3ClientPlugin);
setProofApi("https://apis.matic.network/");

async function main() {
    const parentProvider = new providers.JsonRpcProvider(process.env.GOERLI_URL);
    const childProvider = new providers.JsonRpcProvider(process.env.POLYGON_URL);

    const privateKey = process.env.HARDHAT_PRIVATE_KEY;
    const fromAddress = process.env.HARDHAT_ADDRESS;
    
    const posClient = new POSClient();
    await posClient.init({
        network: "testnet",
        version: "mumbai",
        parent: {
            provider: new Wallet(privateKey, parentProvider),
            defaultConfig: {
                from : fromAddress
            }
        },
        child: {
            provider: new Wallet(privateKey, childProvider),
            defaultConfig: {
                from : fromAddress
            }
        }
    });

    const rootTokenAddress = "";
    const childTokenAddress = "";
    const erc20RootToken = posClient.erc20(rootTokenAddress, true);
    const erc20ChildToken = posClient.erc20(childTokenAddress);

    // count of tokens to withdraw
    const totalTokensToWithdraw = 0;
    const parsedTotalTokensToWithdraw = utils.parseUnits(totalTokensToWithdraw.toString(), 18).toString();

    // start withdraw process
    const withdrawStartResult = await erc20ChildToken.withdrawStart(parsedTotalTokensToWithdraw);
    const withdrawStartTxHash = await withdrawStartResult.getTransactionHash();
    const withdrawStartTxReceipt = await withdrawStartResult.getReceipt();
    console.log(withdrawStartTxHash, withdrawStartTxReceipt);

    // withdraw exit
    const withdrawExitFasterResult = await erc20RootToken.withdrawExitFaster(withdrawStartTxHash);
    const withdrawExitFasterTxHash = await withdrawExitFasterResult.getTransactionHash();
    const withdrawExitFasterTxReceipt = await withdrawExitFasterResult.getReceipt();
    console.log(withdrawExitFasterTxHash, withdrawExitFasterTxReceipt);

    // check exit
    const isExited = await erc20RootToken.isWithdrawExited(withdrawStartTxHash);
    console.log(isExited);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
