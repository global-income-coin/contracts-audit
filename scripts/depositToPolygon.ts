require('dotenv').config();

let { POSClient, use } = require("@maticnetwork/maticjs");
let { Web3ClientPlugin } = require("@maticnetwork/maticjs-ethers");
let { providers, Wallet, utils } = require("ethers");

use(Web3ClientPlugin);

async function main() {
    const parentProvider = new providers.JsonRpcProvider(process.env.GOERLI_URL);
    const childProvider = new providers.JsonRpcProvider(process.env.POLYGON_URL);

    const privateKey = process.env.HARDHAT_PRIVATE_KEY;
    const fromAddress = process.env.HARDHAT_ADDRESS;
    
    const posClient = new POSClient();
    await posClient.init({
        network: "testnet",
        version: "mumbai",
        parent: {
            provider: new Wallet(privateKey, parentProvider),
            defaultConfig: {
                from : fromAddress
            }
        },
        child: {
            provider: new Wallet(privateKey, childProvider),
            defaultConfig: {
                from : fromAddress
            }
        }
    });

    const rootTokenAddress = "";
    const distributorAddress = "";
    const erc20RootToken = posClient.erc20(rootTokenAddress, true);

    // count of tokens to deposit
    const totalTokensToDeposit = 0;
    const parsedTotalTokensToDeposit = utils.parseUnits(totalTokensToDeposit.toString(), 18).toString();

    // approve
    const approveResult = await erc20RootToken.approve(parsedTotalTokensToDeposit);
    const approveTxHash = await approveResult.getTransactionHash();
    const approveTxReceipt = await approveResult.getReceipt();
    console.log(approveTxHash, approveTxReceipt);

    // deposit
    const depositResult = await erc20RootToken.deposit(parsedTotalTokensToDeposit, distributorAddress);
    const depositTxHash = await depositResult.getTransactionHash();
    const depositTxReceipt = await depositResult.getReceipt();
    console.log(depositTxHash, depositTxReceipt);

    // check deposit
    // const isDeposited = await posClient.isDeposited(depositTxHash);
    // console.log(isDeposited);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
