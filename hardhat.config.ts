import * as dotenv from "dotenv";

import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-etherscan";
import "@nomiclabs/hardhat-waffle";
import "@typechain/hardhat";
import "hardhat-gas-reporter";
import "solidity-coverage";
import "@openzeppelin/hardhat-upgrades";
const { utils } = require("ethers");

dotenv.config();

// GLO CONTRACT TASKS

// npx hardhat mint --network goerli --contract contractAddress --to walletAddress --amount numOfTokens
task("mint", "Mint new GLO")
  .addParam("contract", "GLO contract address")
  .addParam("amount", "Number of tokens to mint")
  .addParam("to", "Address to be receive the newly minted tokens")
  .setAction(async (taskArgs, hre) => {
    const gloAddress = taskArgs.contract;
    const amount = taskArgs.amount;
    const to = taskArgs.to;

    const glo = await hre.ethers.getContractAt(
      "GlobalIncomeCoinTest1",
      gloAddress
    );
    const amountWithDecimals = hre.ethers.utils.parseUnits(amount, 18);
    await glo.mint(to, amountWithDecimals);
  });

// CHILD CONTRACT TASKS

// npx hardhat balance --network polygon --contract contractAddress --address walletAddress
task("balance", "Prints GLO balance of given account")
  .addParam("contract", "Global income coin contract address")
  .addParam("address", "Address whose GLO balance you need")
  .setAction(async (taskArgs, hre) => {
    const gloAddress = taskArgs.contract;
    const glo = await hre.ethers.getContractAt("ChildERC20", gloAddress);
    const address = taskArgs.address;

    const gloSupply = await glo.totalSupply();
    const addressBalance = await glo.balanceOf(address);
    console.log("Total supply: ", gloSupply);
    console.log(address, addressBalance);
  });

// REGISTER TASKS

// npx hardhat updateVerificationValiditySeconds --network polygon --contract contractAddress --human-verification-validity-in-seconds humanVerificationValidityInSeconds
task(
  "updateVerificationValiditySeconds",
  "Update human verification validity in seconds"
)
  .addParam("contract", "The register contract address")
  .addParam(
    "humanVerificationValidityInSeconds",
    "Human verification validity in seconds"
  )
  .setAction(async (taskArgs, hre) => {
    const registerAddress = taskArgs.contract;
    const humanVerificationValidityInSeconds =
      taskArgs.humanVerificationValidityInSeconds;

    const register = await hre.ethers.getContractAt(
      "Register",
      registerAddress
    );
    await register.updateVerificationValiditySeconds(
      humanVerificationValidityInSeconds
    );
  });

// npx hardhat register --network polygon --contract contractAddress --human-id humanId --human-address walletAddress --metadata-urls file1url,file2url,file3url --last-verified-time lastVerifiedTimeInEpochSeconds
task("register", "Register and verify human")
  .addParam("contract", "The register contract address")
  .addParam("humanId", "Unique identifier of human")
  .addParam("humanAddress", "Wallet address of human")
  .addParam("metadataUrls", "The metadata files used to verify the human")
  .addParam(
    "lastVerifiedTime",
    "The verification time of the human in epoch seconds"
  )
  .setAction(async (taskArgs, hre) => {
    const registerAddress = taskArgs.contract;
    const humanId = taskArgs.humanId;
    const humanAddress = taskArgs.humanAddress;
    const metadataUrls = taskArgs.metadataUrls.split(",");
    const lastVerifiedTime = taskArgs.lastVerifiedTime;

    const register = await hre.ethers.getContractAt(
      "Register",
      registerAddress
    );
    await register.registerHuman(
      humanId,
      humanAddress,
      metadataUrls,
      lastVerifiedTime
    );
  });

// npx hardhat update --network polygon --contract contractAddress --human-id humanId --human-address walletAddress --metadata-urls file1url,file2url,file3url
task("update", "Update a human")
  .addParam("contract", "The register contract address")
  .addParam("humanId", "Unique identifier of human")
  .addParam("humanAddress", "Wallet address of human")
  .addParam("metadataUrls", "The metadata files used to verify the human")
  .setAction(async (taskArgs, hre) => {
    const registerAddress = taskArgs.contract;
    const humanId = taskArgs.humanId;
    const humanAddress = taskArgs.humanAddress;
    const metadataUrls = taskArgs.metadataUrls.split(",");

    const register = await hre.ethers.getContractAt(
      "Register",
      registerAddress
    );
    await register.updateHuman(humanId, humanAddress, metadataUrls);
  });

// npx hardhat isVerified --network polygon --contract contractAddress --human-id humanId
task("isVerified", "Check if human is verified")
  .addParam("contract", "The register contract address")
  .addParam("humanId", "Unique identifier of human")
  .setAction(async (taskArgs, hre) => {
    const registerAddress = taskArgs.contract;
    const humanId = taskArgs.humanId;

    const register = await hre.ethers.getContractAt(
      "Register",
      registerAddress
    );
    console.log(await register.isHumanIdVerified(humanId));
  });

// npx hardhat reverify --network polygon --contract contractAddress --human-id humanId --last-verified-time lastVerifiedTimeInEpochSeconds
task("reverify", "Reverify a human")
  .addParam("contract", "The register contract address")
  .addParam("humanId", "Unique identifier of human")
  .addParam(
    "lastVerifiedTime",
    "The verification time of the human in epoch(seconds)"
  )
  .setAction(async (taskArgs, hre) => {
    const registerAddress = taskArgs.contract;
    const humanId = taskArgs.humanId;
    const lastVerifiedTime = taskArgs.lastVerifiedTime;

    const register = await hre.ethers.getContractAt(
      "Register",
      registerAddress
    );
    await register.reverifyHuman(humanId, lastVerifiedTime);
  });

// npx hardhat unverify --network polygon --contract contractAddress --human-id humanId
task("unverify", "Unverify a human")
  .addParam("contract", "The register contract address")
  .addParam("humanId", "Unique identifier of human")
  .setAction(async (taskArgs, hre) => {
    const registerAddress = taskArgs.contract;
    const humanId = taskArgs.humanId;

    const register = await hre.ethers.getContractAt(
      "Register",
      registerAddress
    );
    await register.unverifyHuman(humanId);
  });

// DISTRIBUTOR TASKS

// npx hardhat addDrop --network polygon --contract contractAddress --drop-per-address dropPerAddress
task("addDrop", "Add a new drop")
  .addParam("contract", "The distributor contract address")
  .addParam("dropPerAddress", "The quantity of GLO per address to airdrop")
  .setAction(async (taskArgs, hre) => {
    const distributorAddress = taskArgs.contract;
    const dropPerAddress = taskArgs.dropPerAddress;

    const distributor = await hre.ethers.getContractAt(
      "Distributor",
      distributorAddress
    );
    const dropWithDecimals = hre.ethers.utils.parseUnits(dropPerAddress, 18);
    await distributor.addDrop(dropWithDecimals);
  });

// npx hardhat runDrop --network polygon --contract contractAddress --drop-index dropIndex --address-count addressCount
task("runDrop", "Run a drop")
  .addParam("contract", "The distributor contract address")
  .addParam("dropIndex", "The index of the drop")
  .addParam("addressCount", "The number of addresses to initiate drop to")
  .setAction(async (taskArgs, hre) => {
    const distributorAddress = taskArgs.contract;
    const dropIndex = taskArgs.dropIndex;
    const addressCount = taskArgs.addressCount;

    const distributor = await hre.ethers.getContractAt(
      "Distributor",
      distributorAddress
    );
    await distributor.runDrop(dropIndex, addressCount);
  });

// ROLE TASKS

// npx hardhat grantRole --network polygon --contract-address contractAddress --contract-name Distributor --role-name DISTRIBUTOR_ROLE --address walletAddress
// npx hardhat grantRole --network polygon --contract-address contractAddress --contract-name Register --role-name REGISTER_ROLE --address walletAddress
task("grantRole", "Grants given role to given address in given contract")
  .addParam("contractAddress", "The contract address")
  .addParam("contractName", "The contract name")
  .addParam("roleName", "The role name")
  .addParam("address", "Address who should be added to role")
  .setAction(async (taskArgs, hre) => {
    const contractAddress = taskArgs.contractAddress;
    const contractName: "GLO" | "Register" | "Distributor" =
      taskArgs.contractName;
    const roleName = taskArgs.roleName;
    const address = taskArgs.address;

    const contract = await hre.ethers.getContractAt(
      contractName,
      contractAddress
    );
    const role =
      roleName == "DEFAULT_ADMIN_ROLE"
        ? await contract.DEFAULT_ADMIN_ROLE()
        : utils.keccak256(utils.toUtf8Bytes(roleName));
    await contract.grantRole(role, address);
  });

// npx hardhat revokeRole --network polygon --contract-address contractAddress --contract-name Distributor --role-name DISTRIBUTOR_ROLE --address walletAddress
// npx hardhat revokeRole --network polygon --contract-address contractAddress --contract-name Register --role-name REGISTER_ROLE --address walletAddress
task("revokeRole", "Revokes given role from given address in given contract")
  .addParam("contractAddress", "The contract address")
  .addParam("contractName", "The contract name")
  .addParam("roleName", "The role name")
  .addParam("address", "Address who should be removed from role")
  .setAction(async (taskArgs, hre) => {
    const contractAddress = taskArgs.contractAddress;
    const contractName: "GLO" | "Register" | "Distributor" =
      taskArgs.contractName;
    const roleName = taskArgs.roleName;
    const address = taskArgs.address;

    const contract = await hre.ethers.getContractAt(
      contractName,
      contractAddress
    );
    const role =
      roleName == "DEFAULT_ADMIN_ROLE"
        ? await contract.DEFAULT_ADMIN_ROLE()
        : utils.keccak256(utils.toUtf8Bytes(roleName));
    await contract.revokeRole(role, address);
  });

// npx hardhat hasRole --network polygon --contract-address contractAddress --contract-name Distributor --role-name DISTRIBUTOR_ROLE --address walletAddress
// npx hardhat hasRole --network polygon --contract-address contractAddress --contract-name Register --role-name REGISTER_ROLE --address walletAddress
task("hasRole", "Returns if given address has given role in given contract")
  .addParam("contractAddress", "The contract address")
  .addParam("contractName", "The contract name")
  .addParam("roleName", "The role name")
  .addParam("address", "Address whose role must be checked")
  .setAction(async (taskArgs, hre) => {
    const contractAddress = taskArgs.contractAddress;
    const contractName: "GLO" | "Register" | "Distributor" =
      taskArgs.contractName;
    const roleName = taskArgs.roleName;
    const address = taskArgs.address;

    const contract = await hre.ethers.getContractAt(
      contractName,
      contractAddress
    );
    const role =
      roleName == "DEFAULT_ADMIN_ROLE"
        ? await contract.DEFAULT_ADMIN_ROLE()
        : utils.keccak256(utils.toUtf8Bytes(roleName));
    console.log(await contract.hasRole(role, address));
  });

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

const config: HardhatUserConfig = {
  solidity: {
    compilers: [
      {
        version: "0.8.15",
        settings: {
          optimizer: {
            enabled: true,
            runs: 50000,
          },
        },
      },
      {
        version: "0.6.6",
        settings: {
          optimizer: {
            enabled: true,
            runs: 10000,
          },
        },
      },
    ],
  },
  networks: {
    goerli: {
      url: process.env.GOERLI_URL || "",
      accounts:
        process.env.HARDHAT_PRIVATE_KEY !== undefined
          ? [process.env.HARDHAT_PRIVATE_KEY]
          : [],
    },
    polygon: {
      url: process.env.POLYGON_URL || "",
      accounts:
        process.env.HARDHAT_PRIVATE_KEY !== undefined
          ? [process.env.HARDHAT_PRIVATE_KEY]
          : [],
    },
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },
  etherscan: {
    apiKey: process.env.ETHERSCAN_API_KEY,
  },
};

export default config;
