import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
import { Contract, utils } from "ethers";
import { ethers, upgrades } from "hardhat";

const GlobalIncomeCoinContractName = "GLO";
const RegisterContractName = "Register";
const DistributorContractName = "Distributor";

describe("Register tests", function () {
  let admin: SignerWithAddress,
    verifier1: SignerWithAddress,
    verifier2: SignerWithAddress,
    human1: SignerWithAddress,
    human2: SignerWithAddress;
  const human1Id = "human1";
  const human2Id = "human2";
  const nonExistingHumanId = "nonExistingHumanId";

  let glo: Contract;
  let register: Contract;
  let distributor: Contract;

  before(async function () {
    [admin, verifier1, verifier2, human1, human2] = await ethers.getSigners();
  });

  beforeEach(async function () {
    const GlobalIncomeCoin = await ethers.getContractFactory(
      GlobalIncomeCoinContractName
    );
    glo = await upgrades.deployProxy(GlobalIncomeCoin, []);

    const Register = await ethers.getContractFactory(RegisterContractName);
    register = await upgrades.deployProxy(Register, []);

    const Distributor = await ethers.getContractFactory(
      DistributorContractName
    );
    distributor = await upgrades.deployProxy(Distributor, [
      glo.address,
      register.address,
    ]);

    // Update register verification validity to 270 days.
    await register.connect(admin).updateVerificationValiditySeconds(23331600);

    await register
      .connect(admin)
      .grantRole(
        utils.keccak256(utils.toUtf8Bytes("REGISTER_ROLE")),
        verifier1.address
      );
    await register
      .connect(admin)
      .grantRole(
        utils.keccak256(utils.toUtf8Bytes("REGISTER_ROLE")),
        verifier2.address
      );
    await register
      .connect(admin)
      .revokeRole(
        utils.keccak256(utils.toUtf8Bytes("REGISTER_ROLE")),
        admin.address
      );

    await register
      .connect(admin)
      .grantRole(
        utils.keccak256(utils.toUtf8Bytes("REVERIFY_ROLE")),
        verifier1.address
      );
    await register
      .connect(admin)
      .grantRole(
        utils.keccak256(utils.toUtf8Bytes("REVERIFY_ROLE")),
        verifier2.address
      );
    await register
      .connect(admin)
      .revokeRole(
        utils.keccak256(utils.toUtf8Bytes("REVERIFY_ROLE")),
        admin.address
      );

    await register
      .connect(admin)
      .grantRole(
        utils.keccak256(utils.toUtf8Bytes("UNVERIFY_ROLE")),
        verifier1.address
      );
    await register
      .connect(admin)
      .grantRole(
        utils.keccak256(utils.toUtf8Bytes("UNVERIFY_ROLE")),
        verifier2.address
      );
    await register
      .connect(admin)
      .revokeRole(
        utils.keccak256(utils.toUtf8Bytes("UNVERIFY_ROLE")),
        admin.address
      );
  });

  it("Human registration must work", async function () {
    // Check if human1 is not registered.
    expect((await register.humansMap(human1Id)).isVerified).to.equal(false);
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);

    // Human registration should not be possible by a non-verifier.
    await expect(
      register
        .connect(human2)
        .registerHuman(human1Id, human1.address, [], Date.now())
    ).to.be.reverted;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);

    // Human registration should not be possible if lastVerifiedTime = 0
    await expect(
      register.connect(verifier1).registerHuman(human1Id, human1.address, [], 0)
    ).to.be.reverted;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);

    // Human registration should be possible by a verifier.
    expect(
      await register
        .connect(verifier1)
        .registerHuman(human1Id, human1.address, [], Date.now())
    ).to.be.ok;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(true);

    // Human registration should not be possible if human has been previously registered.
    await expect(
      register
        .connect(verifier1)
        .registerHuman(human1Id, human1.address, [], Date.now())
    ).to.be.reverted;
    await expect(
      register
        .connect(verifier2)
        .registerHuman(human1Id, human1.address, [], Date.now())
    ).to.be.reverted;

    expect(await register.getRegisteredHumansCount()).to.equal(1);
  });

  it("Human update must work", async function () {
    // It should revert if human doesnt exist.
    await expect(register.connect(verifier1).updateHuman(nonExistingHumanId)).to
      .be.reverted;

    // Verified human update should not be possible by a admin/non-verifier.
    expect(
      await register
        .connect(verifier1)
        .registerHuman(
          human1Id,
          human1.address,
          ["original1", "original2"],
          1656702394478
        )
    ).to.be.ok;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(true);
    await expect(
      register
        .connect(admin)
        .updateHuman(human1Id, human2.address, ["new1", "new2"])
    ).to.be.reverted;
    await expect(
      register
        .connect(human2)
        .updateHuman(human1Id, human2.address, ["new3", "new4"])
    ).to.be.reverted;

    // Verified human update should not be possible by a verifier different from humans current verifier.
    await expect(
      register
        .connect(verifier2)
        .updateHuman(human1Id, human2.address, ["new5", "new6"])
    ).to.be.reverted;

    expect(await register.getMetadataUrls(human1Id)).to.deep.equal([
      "original1",
      "original2",
    ]);

    // Update should not be possible for a non-registered human.
    await expect(
      register
        .connect(verifier1)
        .updateHuman(nonExistingHumanId, human1.address, ["non-existing-metadata-url-1", "non-existing-metadata-url-2"])
    ).to.be.reverted;

    // Verified human update should be possible by the humans current verifier.
    expect(
      await register
        .connect(verifier1)
        .updateHuman(human1Id, human2.address, ["new7", "new8"])
    ).to.be.ok;
    expect(await register.getMetadataUrls(human1Id)).to.deep.equal([
      "new7",
      "new8",
    ]);

    // Unverify human1
    expect(await register.connect(admin).unverifyHuman(human1Id)).to.be.ok;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);

    // Unverified human update should not be possible by a admin/non-verifier.
    await expect(
      register
        .connect(admin)
        .updateHuman(human1Id, human2.address, ["new9", "new10"])
    ).to.be.reverted;
    await expect(
      register
        .connect(human2)
        .updateHuman(human1Id, human2.address, ["new11", "new12"])
    ).to.be.reverted;

    // Unverified human update should be possible by last verifier of the human.
    expect(
      await register
        .connect(verifier1)
        .updateHuman(human1Id, human2.address, ["new13", "new14"])
    ).to.be.ok;
    expect(await register.getMetadataUrls(human1Id)).to.deep.equal([
      "new13",
      "new14",
    ]);

    // Check if human1 is still unverified.
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);

    // Unverified human update should be possible by a verifier different from humans current verifier.
    expect(
      await register
        .connect(verifier2)
        .updateHuman(human1Id, human2.address, ["new15", "new16"])
    ).to.be.ok;
    expect(await register.getMetadataUrls(human1Id)).to.deep.equal([
      "new15",
      "new16",
    ]);

    expect(await register.getRegisteredHumansCount()).to.equal(1);
  });

  it("Human unverification must work", async function () {
    // It should revert if human doesnt exist.
    await expect(register.connect(admin).unverifyHuman(nonExistingHumanId))
      .to.be.reverted;

    // Any human should be unverifiable by an admin.
    expect(
      await register
        .connect(verifier1)
        .registerHuman(human1Id, human1.address, [], Date.now())
    ).to.be.ok;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(true);
    expect(await register.connect(admin).unverifyHuman(human1Id)).to.be.ok;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);

    // Human should be unverifiable by current verifier.
    expect(
      await register.connect(verifier1).reverifyHuman(human1Id, Date.now())
    ).to.be.ok;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(true);
    expect(await register.connect(verifier1).unverifyHuman(human1Id)).to.be.ok;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);

    // Human should not be unverifiable by a verifier different from humans current verifier.
    expect(
      await register.connect(verifier1).reverifyHuman(human1Id, Date.now())
    ).to.be.ok;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(true);
    await expect(register.connect(verifier2).unverifyHuman(human1Id)).to.be
      .reverted;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(true);

    expect(await register.getRegisteredHumansCount()).to.equal(1);
  });

  it("Human reverification must work", async function () {
    // It should revert if human doesnt exist.
    await expect(register.connect(verifier1).reverifyHuman(nonExistingHumanId, Date.now()))
      .to.be.reverted;
    expect(await register.isHumanIdVerified(nonExistingHumanId)).to.equal(false);

    // Human should not be reverifiable by an admin/non-verifier.
    expect(
      await register
        .connect(verifier1)
        .registerHuman(human1Id, human1.address, [], Date.now())
    ).to.be.ok;
    expect(await register.connect(verifier1).unverifyHuman(human1Id)).to.be.ok;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);
    await expect(register.connect(admin).reverifyHuman(human1Id, Date.now())).to
      .be.reverted;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);
    await expect(register.connect(human2).reverifyHuman(human1Id, Date.now()))
      .to.be.reverted;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);

    // Human should be not reverifiable by a verifier different from humans current verifier.
    await expect(
      register.connect(verifier2).reverifyHuman(human1Id, Date.now())
    ).to.be.reverted;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);

    // It should revert if given lastVerifiedTime == 0.
    await expect(register.connect(verifier1).reverifyHuman(human1Id, 0))
      .to.be.reverted;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(false);

    // Human should be reverifiable by its current verifier.
    expect(
      await register.connect(verifier1).reverifyHuman(human1Id, Date.now())
    ).to.be.ok;
    expect(await register.isHumanIdVerified(human1Id)).to.equal(true);

    expect(await register.getRegisteredHumansCount()).to.equal(1);
  });

  it("getMetadataUrls must work", async function name() {
    // Should return [] for non-existing human.
    expect(await register.getMetadataUrls(nonExistingHumanId)).to.deep.equal([]);

    // Check return correct metadataUrls for existing human.
    expect(
      await register
        .connect(verifier1)
        .registerHuman(human1Id, human1.address, ["metadataUrl1", "metadataUrl2"], Date.now())
    ).to.be.ok;
    expect(await register.getMetadataUrls(human1Id)).to.deep.equal(["metadataUrl1", "metadataUrl2"]);
  });

  it("getAddressesSlice must work", async function name() {
    // endIndex > startIndex must error out.
    await expect(register.getAddressesSlice(3, 2)).to.be.reverted;

    // calling non existing indexes must error out.
    await expect(register.getAddressesSlice(1, 5)).to.be.reverted;

    // Start registering and checking returns.
    expect(
      await register
        .connect(verifier1)
        .registerHuman(human1Id, human1.address, [], Date.now())
    ).to.be.ok;
    expect(await register.getAddressesSlice(0, 1)).to.deep.equal([human1.address]);

    expect(
      await register
        .connect(verifier1)
        .registerHuman(human2Id, human2.address, [], Date.now())
    ).to.be.ok;
    expect(await register.getAddressesSlice(0, 1)).to.deep.equal([human1.address]);
    expect(await register.getAddressesSlice(0, 2)).to.deep.equal([human1.address, human2.address]);

    // Check if unverified humans are returned as address(0).
    expect(await register.connect(verifier1).unverifyHuman(human1Id)).to.be.ok;
    expect(await register.getAddressesSlice(0, 1)).to.deep.equal([ethers.constants.AddressZero]);
    expect(await register.getAddressesSlice(0, 2)).to.deep.equal([ethers.constants.AddressZero, human2.address]);
  });
});

describe("Distribution tests", function () {
  let owner: SignerWithAddress,
    user1: SignerWithAddress,
    user2: SignerWithAddress,
    user3: SignerWithAddress,
    user4: SignerWithAddress;
  let globalIncomeCoin: Contract;
  let register: Contract;
  let distributor: Contract;

  before(async function () {
    [owner, user1, user2, user3, user4] = await ethers.getSigners();
  });

  beforeEach(async function () {
    const GlobalIncomeCoin = await ethers.getContractFactory(
      GlobalIncomeCoinContractName
    );
    globalIncomeCoin = await upgrades.deployProxy(GlobalIncomeCoin, []);

    const Register = await ethers.getContractFactory(RegisterContractName);
    register = await upgrades.deployProxy(Register, []);

    const Distributor = await ethers.getContractFactory(
      DistributorContractName
    );
    distributor = await upgrades.deployProxy(Distributor, [
      globalIncomeCoin.address,
      register.address,
    ]);

    // Update register verification validity to 270 days + 1 hour to take care of possible block timstamp manipulation.
    await register.connect(owner).updateVerificationValiditySeconds(23331600);
  });

  async function registerHuman(
    register: Contract,
    verifier: SignerWithAddress,
    human: SignerWithAddress,
    humanId: string
  ) {
    const metadataUrls = [`url1/${humanId}`, `url2/${humanId}`];
    await register
      .connect(verifier)
      .registerHuman(humanId, human.address, metadataUrls, Date.now());
    return true;
  }

  it("Drops should work correctly", async function () {
    // Register 3 users for drops
    await registerHuman(register, owner, user1, "user1Id");
    await registerHuman(register, owner, user2, "user2Id");
    await registerHuman(register, owner, user3, "user3Id");

    // Check if creating a drop with 0 distrubutions per human fails
    await expect(distributor.addDrop(0)).to.be.reverted;

    // Create new drop
    const drop1 = await distributor.addDrop(3);
    const drop1Receipt = await drop1.wait();
    expect(drop1Receipt.events[0].args[0]).to.equal(0);

    // Try running a drop with no capital
    await expect(distributor.runDrop(0, 2)).to.be.reverted;
    expect(await globalIncomeCoin.balanceOf(distributor.address)).to.equal(0);
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(0);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(0);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(0);

    // Transfer sufficient capital
    await globalIncomeCoin.mint(distributor.address, 10);
    expect(await globalIncomeCoin.balanceOf(distributor.address)).to.equal(10);

    // Check if running a non-existing drop fails
    await expect(distributor.runDrop(1, 1)).to.be.reverted;

    // Check if running a existing drop but for 0 humans fails
    await expect(distributor.runDrop(0, 0)).to.be.reverted;

    expect(await distributor.runDrop(0, 2)).to.be.ok;
    expect(await globalIncomeCoin.balanceOf(distributor.address)).to.equal(4);
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(3);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(3);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(0);

    expect(await distributor.runDrop(0, 2)).to.be.ok;
    expect(await globalIncomeCoin.balanceOf(distributor.address)).to.equal(1);
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(3);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(3);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(3);

    await expect(distributor.runDrop(0, 2)).to.be.reverted;

    // Check if newly registered user doesnt get any past drops
    await registerHuman(register, owner, user4, "user4Id");
    await expect(distributor.runDrop(0, 2)).to.be.reverted;
    expect(await globalIncomeCoin.balanceOf(user4.address)).to.equal(0);

    // Try new drop

    const drop2 = await distributor.addDrop(5);
    const drop2Receipt = await drop2.wait();
    expect(drop2Receipt.events[0].args[0]).to.equal(1);

    await globalIncomeCoin.mint(distributor.address, 20);
    expect(await globalIncomeCoin.balanceOf(distributor.address)).to.equal(21);

    expect(await distributor.runDrop(1, 2)).to.be.ok;
    expect(await globalIncomeCoin.balanceOf(distributor.address)).to.equal(11);
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(8);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(8);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(3);
    expect(await globalIncomeCoin.balanceOf(user4.address)).to.equal(0);

    expect(await distributor.runDrop(1, 1)).to.be.ok;
    expect(await globalIncomeCoin.balanceOf(distributor.address)).to.equal(6);
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(8);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(8);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(8);
    expect(await globalIncomeCoin.balanceOf(user4.address)).to.equal(0);

    expect(await distributor.runDrop(1, 1)).to.be.ok;
    expect(await globalIncomeCoin.balanceOf(distributor.address)).to.equal(1);
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(8);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(8);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(8);
    expect(await globalIncomeCoin.balanceOf(user4.address)).to.equal(5);

    await expect(distributor.runDrop(1, 1)).to.be.reverted;
  });

  it("Check burning permissions", async function () {
    // Give both user1 and user2 10 GLO each
    await globalIncomeCoin.mint(user1.address, 10);
    await globalIncomeCoin.mint(user2.address, 10);

    // Grant only user2 BURNER_ROLE role
    await globalIncomeCoin.grantRole(
      globalIncomeCoin.BURNER_ROLE(),
      user2.address
    );

    // Check if user1 cant burn tokens
    await expect(globalIncomeCoin.connect(user1).burn(10)).to.be.reverted;

    // Check if user2 can burn tokens
    expect(await globalIncomeCoin.connect(user2).burn(10)).to.be.ok;
  });
});

describe("GLO tests", function () {
  let owner: SignerWithAddress,
    user1: SignerWithAddress,
    user2: SignerWithAddress,
    user3: SignerWithAddress,
    user4: SignerWithAddress;
  let globalIncomeCoin: Contract;

  before(async function () {
    [owner, user1, user2, user3, user4] = await ethers.getSigners();
  });

  beforeEach(async function () {
    const GlobalIncomeCoin = await ethers.getContractFactory(
      GlobalIncomeCoinContractName
    );
    globalIncomeCoin = await upgrades.deployProxy(GlobalIncomeCoin, []);
  });

  it("Check burn", async function () {
    // Give both user1 and user2 10 GLO each.
    await globalIncomeCoin.mint(user1.address, 10);
    await globalIncomeCoin.mint(user2.address, 10);

    // Grant only user2 BURNER_ROLE role.
    await globalIncomeCoin.grantRole(
      globalIncomeCoin.BURNER_ROLE(),
      user2.address
    );

    // Check if user1 cant burn tokens.
    await expect(globalIncomeCoin.connect(user1).burn(5)).to.be.reverted;
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(10);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(10);

    // Check if user2 can burn tokens.
    expect(await globalIncomeCoin.connect(user2).burn(5)).to.be.ok;
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(10);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(5);
  });

  it("Check burnFrom", async function () {
    // Give both user1 and user2 10 GLO each.
    await globalIncomeCoin.mint(user1.address, 10);
    await globalIncomeCoin.mint(user2.address, 10);

    // Grant only user2 BURNER_ROLE role.
    await globalIncomeCoin.grantRole(
      globalIncomeCoin.BURNER_ROLE(),
      user2.address
    );

    // Check if user1 cant burnFrom tokens.
    await expect(globalIncomeCoin.connect(user1).burnFrom(user1.address, 5)).to.be.reverted;
    await expect(globalIncomeCoin.connect(user1).burnFrom(user2.address, 5)).to.be.reverted;
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(10);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(10);

    // Check if user2 can burnFrom tokens.
    await expect(globalIncomeCoin.connect(user2).burnFrom(user1.address, 5)).to.be.reverted;
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(10);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(10);
    expect(await globalIncomeCoin.connect(user1).approve(user2.address, 5)).to.be.ok;
    expect(await globalIncomeCoin.connect(user2).burnFrom(user1.address, 5)).to.be.ok;
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(5);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(10);
  });

  it("Check transferMany", async function () {
    // Give both user1 1000 GLO.
    await globalIncomeCoin.mint(user1.address, 1000);

    // Check initial balances.
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(1000);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(0);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(0);
    expect(await globalIncomeCoin.balanceOf(user4.address)).to.equal(0);

    // Check if transfermany worked.
    expect(
      await globalIncomeCoin
        .connect(user1)
        .transferMany([user2.address, user3.address, user4.address], 300)
    ).to.be.ok;
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(100);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(300);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(300);
    expect(await globalIncomeCoin.balanceOf(user4.address)).to.equal(300);

    // Check if transfermany fails if not enough balance.
    await expect(
      globalIncomeCoin
        .connect(user1)
        .transferMany([user2.address, user3.address, user4.address], 35)
    ).to.be.reverted;
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(100);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(300);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(300);
    expect(await globalIncomeCoin.balanceOf(user4.address)).to.equal(300);

    // Check if transfermany worked with few empty addresses.
    expect(
      await globalIncomeCoin
        .connect(user1)
        .transferMany(
          [
            ethers.constants.AddressZero,
            user2.address,
            ethers.constants.AddressZero,
            user3.address,
            ethers.constants.AddressZero,
            user4.address,
          ],
          33
        )
    ).to.be.ok;
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(1);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(333);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(333);
    expect(await globalIncomeCoin.balanceOf(user4.address)).to.equal(333);

    // Check if transfermany worked with all empty addresses.
    expect(
      await globalIncomeCoin
        .connect(user1)
        .transferMany(
          [
            ethers.constants.AddressZero,
            ethers.constants.AddressZero,
            ethers.constants.AddressZero,
          ],
          100
        )
    ).to.be.ok;
    expect(await globalIncomeCoin.balanceOf(user1.address)).to.equal(1);
    expect(await globalIncomeCoin.balanceOf(user2.address)).to.equal(333);
    expect(await globalIncomeCoin.balanceOf(user3.address)).to.equal(333);
    expect(await globalIncomeCoin.balanceOf(user4.address)).to.equal(333);
  });
});
